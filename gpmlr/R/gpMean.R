#' Mean functions in \code{\link{gpmlr}}
#'
#' Mean S4 object constructor functions to be use by Gaussian process functions.
#' Two different kinds of mean function objects are implemented: simple and
#' composite.
#'
#' Simple mean functions include: MeanZero, MeanConst, MeanLinear.
#'
#' Composite mean functions include: MeanSum.
#'
#' See \code{\linkS4class{Mean}} for more information.
#'
#' @param mLst list of mean function handles for composition, only appear
#'   composite mean funtions.
#' @param hyp numeric vector of hyper-parameters
#' @param x   n by D matrix of data
#' @param dodrv logical value indicating whether to calculate and set the
#'   directional derivative slot of the returned S4 Mean object
#'
#' @return Returns an S4 Mean object \code{me} of the class same as the function
#'   name.\cr
#'   If the argument \code{hyp} is provided, it will be copied to the
#'   \code{me@hyp} slot.\cr
#'   If both the argument \code{hyp} and \code{x} is provided, the \code{me@m}
#'   slot will be set to the mean vector calculated.\cr
#'   If the argument \code{dodrv} is \code{TRUE}, the \code{me@dm} slot will be
#'   set to the directional derivative function.\cr
#'
#' @name gpMean
NULL
