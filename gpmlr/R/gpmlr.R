#' gpmlr: Gaussian processes for machine learning in R
#'
#' The gpmlr package provides tools for Gaussian process regression and
#' classification. It is base on the GPML matlab/octave code version 4.0
#' developed by Carl Edward Rasmussen and Hannes Nickisch 2016-10-19.
#'
#' The main user function is \code{\link{gp}}, which does Gaussian process
#' inference or prediction with the assitance of the
#' \code{\link[stats]{nlm}} function and the following 4 catagories of
#' objects:
#' \enumerate{
#'   \item \code{\link[=gpMean]{Mean}} function S4 objects
#'   \item \code{\link[=gpCovariance]{Covariance}} function S4 objects
#'   \item \code{\link{likelihood}} functions
#'   \item \code{\link{inference}} method functions
#' }
#'
#' @docType package
#' @name gpmlr
NULL
