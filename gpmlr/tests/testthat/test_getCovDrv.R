library(gpmlr)
library(stats)
library(testthat)

context("getCovDrv queries")

# random dataset
n <- 5
D <- 3
data_x <- matrix(rnorm(n * D), ncol = D)
data_xs <- matrix(rnorm(3 * D), ncol = D)

## construct cov objects
hyp0 <- numeric()
c0 <- CovZero(hyp0)

L <- runif(D)
sf <- 2
hypga <- log(c(L, sf))
cga <- CovSEArd(hyp = hypga)

ell <- 0.9
hypgi <- log(c(ell, sf))
cgi <- CovSEIso(hyp = hypgi)

hypmi <- log(c(ell, sf))
cmi <- CovMaternIso(d = 3, hyp = hypmi)

hypsc <- c(log(3), hypgi) # scale by 9
csc <- CovScale(cov = list(CovSEIso), hyp = hypsc)

hypsu <- c(hyp0, hypga, hypmi)
csu <- CovSum(list(CovZero, CovSEArd, CovMaternIso), hyp = hypsu)

expect_gCD_sameClass <- function(.Obj, x = data_x, z = data_xs) {
  .Obj <- getCov(.Obj, x, z)
  expect_is(getCovDrv(.Obj), is(.Obj))
}

test_that("getCov returns object of the same class", {
  expect_gCD_sameClass(c0, z = NULL)
  expect_gCD_sameClass(cga, z = NULL)
  expect_gCD_sameClass(cgi, z = NULL)
  expect_gCD_sameClass(cmi, z = NULL)
  expect_gCD_sameClass(csc, z = NULL)
  expect_gCD_sameClass(csu, z = NULL)

  expect_gCD_sameClass(c0)
  expect_gCD_sameClass(cga)
  expect_gCD_sameClass(cgi)
  expect_gCD_sameClass(cmi)
  expect_gCD_sameClass(csc)
  expect_gCD_sameClass(csu)
})

# [To do] more detailed interface tests
# [To do] data tests
