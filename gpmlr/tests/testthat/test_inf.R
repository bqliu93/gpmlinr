library(gpmlr)
library(stats)
library(testthat)

context("inference functions")

## random data set
ntr <- 50
nte <- 1e4
xtr <- 10 * sort(runif(ntr))
# the true underlying function
f <- function(x) sin(x) + sqrt(x)
sn <- 0.2
ytr <-f(xtr) + rnorm(ntr) * sn
# outliers
i <- sample(1:ntr)
nout <- 3
ytr[i[1:nout]] = 5
xte <- seq(0, 10, length.out = 1e4)

covFun <- CovSEIso
sf <- 1
ell <- 0.4
meanFun <- list(MeanSum, list(MeanLinear, MeanConst))
a <- 1 / 5
b <- 1
# hyp order must be mean, cov, lik
hyp0 <- list(mean = c(a, b), cov = log(c(ell, sf)))

Ncg <- 50

likFun <- likGauss
hyp0$lik <- log(sn)
inf <- infGaussLik

infRes <- inf(hyp0, meanFun, covFun, likFun, xtr, ytr)

test_that("inference returns post nlZ and dnlZ", {
  expect_false(is.null(infRes$post))
  expect_false(is.null(infRes$post$alpha))
  expect_false(is.null(infRes$post$sW))
  expect_false(is.null(infRes$post$L))
  expect_false(is.null(infRes$nlZ))
  expect_false(is.null(infRes$dnlZ))
  expect_true(is.null(infRes$ss))
})

infRes <- inf(hyp0, meanFun, covFun, likFun, xtr, ytr, nargout = 1)
test_that("inference returns post only when nargout is 1", {
  expect_false(is.null(infRes$alpha))
  expect_false(is.null(infRes$sW))
  expect_false(is.null(infRes$L))
  expect_true(is.null(infRes$post))
})

# [To do] more detailed interface tests
# [To do] data tests
