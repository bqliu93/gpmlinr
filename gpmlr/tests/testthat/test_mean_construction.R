library(gpmlr)
library(testthat)

context("Mean object construction")

test_that("correct class is constructed with minimal input to constructor", {
  expect_is(MeanZero(), "MeanZero")
  expect_is(MeanConst(), "MeanConst")
  expect_is(MeanLinear(), "MeanLinear")
  expect_is(MeanSum(list()), "MeanSum")
})

test_that("mean objects inherits class 'Mean'", {
  expect_is(MeanZero(), "Mean")
  expect_is(MeanConst(), "Mean")
  expect_is(MeanLinear(), "Mean")
  expect_is(MeanSum(list()), "Mean")
})

test_that("Constructor constructs identical objects as new()", {
  expect_identical(MeanZero(), new("MeanZero"))
  expect_identical(MeanConst(), new("MeanConst"))
  expect_identical(MeanLinear(), new("MeanLinear"))
  expect_identical(MeanSum(list()), new("MeanSum"))
})


# [To do] more detailed interface tests
# [To do] data tests
